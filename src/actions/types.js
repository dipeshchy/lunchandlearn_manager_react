export const FETCH_TALKS = 'FETCH_TALKS';
export const FETCH_INDIVIDUAL_TALK = 'FETCH_INDIVIDUAL_TALK';
export const FETCH_RECENT_TALKS = 'FETCH_RECENT_TALKS';
export const FETCH_UPCOMING_TALKS = 'FETCH_UPCOMING_TALKS';
export const NEW_TALK = 'NEW_TALK';
export const SEARCH_TALK = 'SEARCH_TALK';
export const DELETE_TALK = 'DELETE_TALK';
export const EDIT_TALK = 'EDIT_TALK';