import {
  FETCH_TALKS,
  NEW_TALK,
  FETCH_UPCOMING_TALKS,
  FETCH_RECENT_TALKS,
  FETCH_INDIVIDUAL_TALK,
  SEARCH_TALK,
  DELETE_TALK,
  EDIT_TALK
} from "./types";
import axios from 'axios';

export const fetchTalks = () => dispatch => {
  axios.get("/api/v1/talks")
    .then(res =>
      dispatch({
        type: FETCH_TALKS,
        payload: res.data
      })
    );
};

export const searchTalks = searchTerm => dispatch => {
  axios.get(`/api/v1/search/${searchTerm}`)
    .then(talks =>
      dispatch({
        type: SEARCH_TALK,
        payload: talks.data
      })
    );
};

export const fetchIndividualTalk = id => dispatch => {
  axios.get(`/api/v1/talks/${id}`)
    .then(talk =>
      dispatch({
        type: FETCH_INDIVIDUAL_TALK,
        payload: talk.data
      })
    );
};


export const deleteTalk = id => dispatch => {
  axios.delete(`/api/v1/talks/${id}`)
    .then(res =>
      dispatch({
        type: DELETE_TALK,
        id: id
      })
    );
};

export const fetchUpcomingTalks = () => dispatch => {
  axios.get("/api/v1/upcoming_talks")
    .then(res =>
      dispatch({
        type: FETCH_UPCOMING_TALKS,
        payload: res.data
      })
    );
};

export const fetchRecentTalks = () => dispatch => {
  axios.get("/api/v1/recent_talks")
    .then(talks =>
      dispatch({
        type: FETCH_RECENT_TALKS,
        payload: talks.data
      })
    );
};

export const createTalk = (title, location, date, description) => dispatch => {
  fetch("/api/v1/talks", {
    method: "POST",
    body: JSON.stringify({
      title: title,
      location: location,
      date: date,
      description: description
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })
    .then(res => res.json())
    .then(talk =>
      dispatch({
        type: NEW_TALK,
        payload: talk
      })
    );
};


export const editTalk = (id,title, location, date, description) => dispatch => {
  fetch(`/api/v1/talks/${id}`, {
    method: "PUT",
    body: JSON.stringify({
      title: title,
      location: location,
      date: date,
      description: description
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })
    .then(res => res.json())
    .then(talk =>
      dispatch({
        type: EDIT_TALK,
        payload: talk
      })
    );
};

