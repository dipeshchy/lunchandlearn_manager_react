import React, { Component } from 'react';

class NotFound extends Component {
  render() {
    return (
      <div className="text-center mt-5 text-danger">
        <h3>Oops! Page not found!</h3>
      </div>
    );
  }
}

export default NotFound;