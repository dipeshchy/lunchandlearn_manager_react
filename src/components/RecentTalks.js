import React, { Component } from "react";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchRecentTalks } from '../actions/talkActions';
import Table from "./Table";

class RecentTalks extends Component {

  componentDidMount() {
    this.props.fetchRecentTalks();
  }

  render() {
    return (
      <div>
        <div className="card">
          <h2 className="card-header">Recent Talks</h2>
          <Table talks={this.props.talks} />
        </div>
      </div>
    );
  }
}
RecentTalks.propTypes = {
  fetchRecentTalks: PropTypes.func.isRequired,
  talks: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  talks: state.talks.items
});

export default connect(mapStateToProps, { fetchRecentTalks })(RecentTalks);
