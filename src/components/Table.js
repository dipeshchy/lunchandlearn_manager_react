import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { deleteTalk } from "../actions/talkActions";
const moment = require("moment");

class Table extends Component {
  deleteClick = id =>{
    this.props.deleteTalk(id);
  }
  render() {
    return (
      <div className="card-body">
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Date</th>
              <th>Talk Title</th>
              {/* <th>Speaker</th> */}
              <th>Location</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.props.talks.map(talk => (
              <tr key={talk.id}>
                <td>{moment(`${talk.date}`).format("MMMM Do YYYY")}</td>
                <td>{talk.title}</td>
                <td>{talk.location}</td>
                <td>
                  <Link
                    to={`/talks/show/${talk.id}`}
                    className="btn btn-primary btn-sm"
                  >
                    View
                  </Link>
                  <Link
                    to={`/talks/edit/${talk.id}`}
                    className="btn btn-warning btn-sm ml-2"
                  >
                    Edit
                  </Link>
                  <button onClick={()=>this.deleteClick(talk.id)} className="btn btn-sm btn-danger ml-2">Delete</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default connect(
  null,
  { deleteTalk }
)(Table);
