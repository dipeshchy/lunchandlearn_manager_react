import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { searchTalks } from "../actions/talkActions";
import { Link } from "react-router-dom";
const moment = require("moment");

class SearchResult extends Component {
  componentDidMount() {
    this.props.searchTalks(
      new URLSearchParams(this.props.location.search).get("params")
    );
  }

  render() {
    const query = new URLSearchParams(this.props.location.search);
    const searchValue = query.get("params");
    const { talks } = this.props;
    return (
      <div className="mt-2">
        <h5 className="text-info">Showing Results for {searchValue}</h5>
        {talks.map(talk => (
          <div key={talk.id} className="card mb-2">
            <div className="card-body">
              <h3>Title: {talk.title}</h3>
              <h5>Date : {moment(`${talk.date}`).format("MMMM Do YYYY")} </h5>
              <h5>Location : {talk.location} </h5>
            </div>
            <div className="card-footer">
            <Link to={`/talks/show/${talk.id}`} className="btn btn-primary btn-sm">View</Link>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
SearchResult.propTypes = {
  searchTalks: PropTypes.func.isRequired,
  talks: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  talks: state.talks.items
});

export default connect(
  mapStateToProps,
  { searchTalks }
)(SearchResult);
