import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchTalks } from "../actions/talkActions";
import Table from "./Table";

class Home extends Component {
  componentDidMount() {
    this.props.fetchTalks();
  }

  render() {
    return (
      <div>
        <div className="jumbotron">
          <h2 className="text-center text-info">Lunch And Learn Manager</h2>
          <p>
            Get all information about all the talk events happening near you
            where you can eat and learn at one place and meet incredible peoples
            from different fields.
          </p>
        </div>
        <div className="card">
          <h2 className="card-header">All Talks</h2>
          <Table talks={this.props.talks} />
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  fetchTalks: PropTypes.func.isRequired,
  talks: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  talks: state.talks.items
});

export default connect(
  mapStateToProps,
  { fetchTalks }
)(Home);
