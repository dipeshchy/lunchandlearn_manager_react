import React, { Component } from "react";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchUpcomingTalks } from '../actions/talkActions';
import Table from "./Table";

class UpcomingTalks extends Component {

  componentDidMount() {
    this.props.fetchUpcomingTalks();
  }

  render() {
    return (
      <div>
        <div className="card">
          <h2 className="card-header">Upcoming Talks</h2>
          <Table talks={this.props.talks} />
        </div>
      </div>
    );
  }
}
UpcomingTalks.propTypes = {
  fetchUpcomingTalks: PropTypes.func.isRequired,
  talks: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  talks: state.talks.items
});

export default connect(mapStateToProps, { fetchUpcomingTalks })(UpcomingTalks);
