import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ""
    }
  }

  handleChange = (e) => {
    this.setState({ search: e.target.value })
  }

  onFormSubmit = e => {
    e.preventDefault();
    // console.log(this.state.search);
    // console.log(this.props.history)
    this.setState({ search: "" })
    this.props.history.push(`/search?params=${this.state.search}`);
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-primary mt-2">
          <Link to="/" className="navbar-brand text-white">
            Lunch&Learn Manager
          </Link>

          <form
            className="form-inline my-2 ml-5 my-lg-0"
          >
            <input
              className="form-control mr-sm-2"
              type="search"
              name="search"
              onChange={this.handleChange}
              placeholder="Search Talk"
              aria-label="Search"
              value={this.state.search}
            />
            <button onClick={this.onFormSubmit} className="btn btn-success my-2 my-sm-0" >Search</button>
          </form>

          <div className="collapse navbar-collapse">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to="/" className="nav-link text-white">
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/talks/schedule" className="nav-link text-white">
                  Schedule a Talk
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/talks/upcoming" className="nav-link text-white">
                  Upcoming Talks
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/talks/recent" className="nav-link text-white">
                  Recent Talks
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default withRouter(Navbar);
