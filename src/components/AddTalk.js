import React, { Component } from "react";
import { createTalk } from "../actions/talkActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";

class AddTalk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      location: "",
      date: null,
      description: ""
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  formSubmit = e => {
    e.preventDefault();
    this.props.createTalk(
      this.state.title,
      this.state.location,
      this.state.date,
      this.state.description
    );
    this.props.history.push(`/`);
  };

  render() {
    return (
      <div className="card">
        <h4 className="card-header">Schedule a Talk Event</h4>
        <div className="card-body">
          <form onSubmit={this.formSubmit} className="form-group">
            <div className="form-group">
              <label>Title:</label>
              <input
                type="text"
                className="form-control"
                name="title"
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label>Location:</label>
              <input
                type="text"
                className="form-control"
                name="location"
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label>Date:</label>
              <input
                type="date"
                className="form-control"
                name="date"
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label>Description:</label>
              <textarea
                name="description"
                onChange={this.handleChange}
                className="form-control"
              ></textarea>
            </div>
            <input
              type="submit"
              className="btn btn-success"
              value="Schedule Talk"
            />
          </form>
        </div>
      </div>
    );
  }
}

AddTalk.propTypes = {
  createTalk: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  talks: state.talks.items
});

export default connect(
  mapStateToProps,
  { createTalk }
)(AddTalk);
