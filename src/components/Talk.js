import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchIndividualTalk, deleteTalk } from "../actions/talkActions";
import { Link } from "react-router-dom";
const moment = require("moment");

class Talk extends Component {
  componentDidMount() {
    this.props.fetchIndividualTalk(this.props.match.params.id);
  }

  deleteClick = id =>{
    this.props.deleteTalk(id);
    this.props.history.push("/")
  }

  render() {
    const { talk } = this.props;
    return (
      <div className="card">
        <h4 className="card-header">{talk.title} Event</h4>
        <div className="card-body">
          <h5>Title : {talk.title} </h5>
          <h5>Date : {moment(`${talk.date}`).format("MMMM Do YYYY")} </h5>
          <h5>Location : {talk.location} </h5>
          <h5>Description : {talk.description} </h5>
        </div>
        <div className="card-footer">
          <button
            onClick={() => this.props.history.goBack()}
            to="/"
            className="btn btn-secondary"
          >
            Back
          </button>
          <Link
            to={`/talks/edit/${talk.id}`}
            className="btn btn-warning ml-2"
          >
            Edit
          </Link>
          <button onClick={()=>this.deleteClick(talk.id)} className="btn btn-danger ml-2">Delete</button>
        </div>
      </div>
    );
  }
}

Talk.propTypes = {
  fetchIndividualTalk: PropTypes.func.isRequired,
  talk: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  talk: state.talks.item
});

export default connect(
  mapStateToProps,
  { fetchIndividualTalk, deleteTalk }
)(Talk);
