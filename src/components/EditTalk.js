import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {get} from "lodash";
import { fetchIndividualTalk, editTalk } from "../actions/talkActions";
const moment = require("moment");


class EditTalk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      location: "",
      date: moment(get(props, "talk.date", new Date())).format("YYYY-MM-DD"),
      description: ""
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  formSubmit = e => {
    e.preventDefault();
    this.props.editTalk(
      this.props.match.params.id,
      this.state.title,
      this.state.location,
      this.state.date,
      this.state.description
    );
    this.props.history.push(`/`);
  };

  componentDidMount() {
    this.props.fetchIndividualTalk(this.props.match.params.id);
  }

  render() {
    const { talk } = this.props;
    // console.log(moment(talk.date).format("YYYY-MM-DD"))
    // console.log(this.state);
    return (
      <div className="card">
        <h4 className="card-header">Edit Talk</h4>
        <div className="card-body">
          <form onSubmit={this.formSubmit} className="form-group">
            <div className="form-group">
              <label>Title:</label>
              <input
                type="text"
                className="form-control"
                name="title"
                onChange={this.handleChange}
                defaultValue={talk.title}
              />
            </div>
            <div className="form-group">
              <label>Location:</label>
              <input
                type="text"
                className="form-control"
                name="location"
                onChange={this.handleChange}
                defaultValue={talk.location}
              />
            </div>
            <div className="form-group">
              <label>Date:</label>
              <input
                type="date"
                className="form-control"
                name="date"
                onChange={this.handleChange}
                value={this.state.date}
              />
            </div>
            <div className="form-group">
              <label>Description:</label>
              <textarea
                name="description"
                onChange={this.handleChange}
                className="form-control"
                defaultValue={talk.description}
               ></textarea>
            </div>
            <input
              type="submit"
              className="btn btn-success"
              value="Update Talk"
            />
          </form>
        </div>
      </div>
    );
  }
}

EditTalk.propTypes = {
  fetchIndividualTalk: PropTypes.func.isRequired,
  editTalk: PropTypes.func.isRequired,
  talk: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  talk: state.talks.item
});

export default connect(
  mapStateToProps,
  { fetchIndividualTalk, editTalk  }
)(EditTalk);
