import { combineReducers } from 'redux';
import talkReducer from './talkReducer';

export default combineReducers({
  talks: talkReducer
});