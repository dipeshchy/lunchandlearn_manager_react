import {
  FETCH_TALKS,
  NEW_TALK,
  FETCH_RECENT_TALKS,
  FETCH_UPCOMING_TALKS,
  FETCH_INDIVIDUAL_TALK,
  SEARCH_TALK,
  DELETE_TALK,
  EDIT_TALK
} from "../actions/types";

const initialState = {
  items: [],
  item: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_TALKS:
      return {
        ...state,
        items: action.payload
      };
    case SEARCH_TALK:
      return {
        ...state,
        items: action.payload
      };
    case FETCH_RECENT_TALKS:
      return {
        ...state,
        items: action.payload
      };
    case FETCH_UPCOMING_TALKS:
      return {
        ...state,
        items: action.payload
      };
    case FETCH_INDIVIDUAL_TALK:
      return {
        ...state,
        item: action.payload
      };
    case DELETE_TALK:
      return {
        ...state,
        items: state.items.filter((data) => data.id !== action.id)
      };
    case NEW_TALK:
      return {
        ...state,
        item: action.payload
      };
    case EDIT_TALK:
      return {
        ...state,
        item: action.payload
      };
    default:
      return state;
  }
}
