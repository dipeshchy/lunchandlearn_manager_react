import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Home from "./components/Home";
import RecentTalks from "./components/RecentTalks";
import UpcomingTalks from "./components/UpcomingTalks";
import AddTalk from "./components/AddTalk";
import NotFound from "./components/NotFound";
import Talk from "./components/Talk";
import SearchResult from "./components/SearchResult";
import EditTalk from "./components/EditTalk";

function App() {
  return (
    <Router>
      <div>
        <Navbar />
        <div className="mt-3 container">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/talks/recent" component={RecentTalks} />
            <Route path="/talks/upcoming" component={UpcomingTalks} />
            <Route path="/talks/schedule" component={AddTalk} />
            <Route path="/talks/show/:id" component={Talk} />
            <Route path="/talks/edit/:id" component={EditTalk} />
            <Route path="/search" component={SearchResult} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
